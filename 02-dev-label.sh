#!/bin/bash

set -eu

function get_label {
    local dev_path=$1
    sudo parted -m $dev_path print \
        | grep "^$dev_path:" \
        | cut -d: -f6
}

# SCSIデバイス、仮想デバイスのみ探す。
# md, vd は今は見ない。必要になったら見る。
cat /mnt/backup/dev-path.txt \
    | while read dev_path; do echo "$dev_path $(get_label $dev_path)"; done \
    | sudo tee /mnt/backup/dev-path-label.txt

exit 0
