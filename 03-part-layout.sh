#!/bin/bash

set -eu

cat /mnt/backup/dev-path-label.txt \
    | while read dev_path label; do
        echo "===>>> $dev_path <<<==="
        d=$(echo $dev_path | grep -o '[^/]*$')
        if [[ $label == "msdos" ]]; then
            sfdisk -d $dev_path | sudo tee /mnt/backup/sfdisk-d-${d}.txt
        elif [[ $label == "gpt" ]]; then
            sgdisk -b /mnt/backup/sgdisk-b-${d}.bin $dev_path
        fi
      done

exit 0
