#!/bin/bash

set -eu

df -h \
    | grep -v tmpfs \
    | grep -v ^Filesystem \
    | grep -v /mnt \
    | awk '{print $1}' \
    | while read dpath; do
        d=$(echo $dpath | grep -o '[^/]*$')
        xfsdump -l0 - $dpath | gzip -9 >/mnt/${d}.xfsdump.gz
      done

exit 0
