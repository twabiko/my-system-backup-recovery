#!/bin/bash

set -eu

function get_label {
    local dev_path=$1
    sudo parted -m $dev_path print \
        | grep "^$dev_path " \
        | cut -d: -f6
}

# SCSIデバイス、仮想デバイスのみ探す。
# md, vd は今は見ない。必要になったら見る。
cat /proc/partitions \
    | awk '{print $4}' \
    | grep -e ^sd -e ^vd \
    | grep -v '[0-9]' \
    | sed -e 's!^!/dev/!' \
    | sudo tee /mnt/backup/dev-path.txt

exit 0
